########################################
## INCLUDES FOR STAGING && PRODUCTION ##
########################################
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_cpt_nav_breadcrumb/Configuration/TypoScript/Constants/Production" extensions="txt">

##############################
## OVERRIDE FOR DEVELOPMENT ##
##############################
[globalString = ENV:HTTP_HOST=development.*]
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:hive_cpt_nav_breadcrumb/Configuration/TypoScript/Constants/Development" extensions="txt">
[global]