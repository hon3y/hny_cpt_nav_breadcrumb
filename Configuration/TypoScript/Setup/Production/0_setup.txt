#################################
## NAVIGATION BREADCRUMB SETUP ##
#################################

[globalVar = LIT:1 = {$plugin.tx_hive_cpt_nav_breadcrumb.settings.lib.navigation.bBreadcrumb}]
	lib.navigation.breadcrumb = COA
	lib.navigation.breadcrumb {

        5 = LOAD_REGISTER
        5  {
            classNav.cObject = TEXT
            classNav.cObject {
                field = classNav
                ifEmpty = {$plugin.tx_hive_cpt_nav_breadcrumb.settings.lib.navigation.class.nav}
            }
            classDiv.cObject = TEXT
            classDiv.cObject {
                field = classDiv
                ifEmpty = {$plugin.tx_hive_cpt_nav_breadcrumb.settings.lib.navigation.class.div}
            }

			cachePostfix.cObject = TEXT
            cachePostfix.cObject {
                field = cachePostfix
                ifEmpty = {$plugin.tx_hive_cpt_nav_breadcrumb.settings.lib.navigation.cache.postfix}
            }
        }

		10 = HMENU
		10 {
			special = rootline
            special.range = 0|-1
			includeNotInMenu = 1
			1 = TMENU
			1 {
                required = 1
        		stdWrap.dataWrap = <nav class="{register:classNav}"><div class="{register:classDiv}"><ol class="breadcrumb">|</ol></div></nav>
				noBlur = 1
				target = _self
				NO = 1
				NO {
					stdWrap.field = nav_title // title
					ATagTitle.field = nav_title // title
					wrapItemAndSub = <li>|</li>
				}

				CUR = 1
				CUR {
					stdWrap.field = nav_title // title
					doNotLinkIt = 1
					wrapItemAndSub = <li class="active">|</li>
				}
			}
		}
	}
[end]

[treeLevel = {$plugin.tx_hive_cpt_nav_breadcrumb.settings.lib.navigation.sTreeLevel}]
	lib.navigation.breadcrumb >
[end]

# Caching for Menu
[globalVar = LIT:1 = {$plugin.tx_hive_cpt_nav_breadcrumb.settings.production.optional.active}]
lib.navigation.breadcrumb {
    stdWrap {
        cache {
            /**
             * Hier wird die aktuelle Sprache benutzt, um einen Inhalt pro Sprache
             * für alle Seiten zu cachen.
             */
            key = tx_hive_cpt_nav_breadcrumb_{register:cachePostfix}_{page:uid}_{TSFE:sys_language_uid}
            key.insertData = 1

            // Mit den selbstgewählten Tags kann der Cache später gezielt geleert werden.
            tags = tx_hive_cpt_nav_breadcrumb_{register:cachePostfix}_{page:uid}_{TSFE:sys_language_uid}
            tags.insertData = 1

            /**
             * Wenn eine neue Seite generiert wird, ist das Kontaktmenü max. eine Stunde alt.
             * Wenn die Seite alle 24 Stunden neu generiert wird, könnte das Kontaktmenü
             * also auch über einen Tag alt sein!
             */
            # 3600 x 24 x 30
            lifetime = 2592000
        }
    }

    4 = TEXT
    4 {
        data = date: dmyHis
        wrap = <!--CACHED_|-->
    }
}
[global]
